package com.azc.tictactoe;

public class TicTacToeGame {

    private Character[][] board={{'\0','\0','\0'},{'\0','\0','\0'},{'\0','\0','\0'}};
    private Character lastPlayer='\0';

    private TicTacToeDAO ticTacToeDAO;

    public String play(int x, int y) {
        checkAxis(x);
        checkAxis(y);
        lastPlayer=nextPlayer();
        setBoard(x,y);
        saveMove(x,y);
        if(isWin()){
            return lastPlayer+" is Winner";
        }else if(isDraw()){
            return "The result is draw";
        }else{
            return "No Winner";
        }
    }

    private boolean isWin() {
        int playerTotal=3*lastPlayer;

        if(checkDioganals(playerTotal)||checkHandVStates(playerTotal))
            return true;
        else
            return false;
    }

    private boolean checkHandVStates(int playerTotal){
        for (int i=0;i<board.length;i++){
            int horizontal=board[0][i] + board[1][i]+ board[2][i];
            int vertical=board[i][0] + board[i][1]+ board[i][2];
            System.out.println("v="+vertical+"  h="+ horizontal+" pt="+playerTotal);
            if(horizontal==playerTotal || vertical==playerTotal){
                return true;
            }
        }
        return false;
    }

    private boolean checkDioganals(int playerTotal){
        int dioganal1=board[0][0]+ board[1][1]+board[2][2];
        int dioganal2=board[0][2]+ board[1][1]+board[2][0];
        if(dioganal1==playerTotal||dioganal2==playerTotal)
            return true;
        else
            return false;
    }

    private void setBoard(int x,int y) {
        if(board[x-1][y-1]!='\0'){
            throw new RuntimeException("Board is occupied");
        }else{
            board[x-1][y-1]=lastPlayer;
        }
    }


    private void checkAxis(int axis){
        if(axis>3||axis<1){
            throw new RuntimeException("Outside of the board");
        }
    }

    public char nextPlayer() {
        if(lastPlayer=='X'){
            return  'O';
        }
        return 'X';
    }

    public boolean isDraw() {
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board.length; y++) {
                if (board[x][y] == '\0') {
                    return false;
                }
            }
        }
        return true;
    }

    public void saveMove(int x,int y){
        TicTacToeBean lastMove = ticTacToeDAO.getLastMove();
        ticTacToeDAO.saveMove(new TicTacToeBean(lastMove.getMove()+1,lastPlayer,x,y));
    }
}
