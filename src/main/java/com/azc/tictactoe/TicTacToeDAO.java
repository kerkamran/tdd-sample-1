package com.azc.tictactoe;

public interface TicTacToeDAO {
    void saveMove(TicTacToeBean bean);
   // void saveMove();
    void svaeBoardState(Character[][] boardState);
    TicTacToeBean[] getGameHistory();
    char getNextPlayer();
    TicTacToeBean getLastMove();
}
